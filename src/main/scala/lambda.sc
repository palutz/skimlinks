sealed trait Expr
case class Var(name: String) extends Expr
case class Lambda(arg: Expr, body: Expr) extends Expr
case class Apply(fun: Expr, arg: Expr) extends Expr

val id = Lambda(Var("x"), Var("x"))
val succ = Lambda(Var("n"),
              Lambda(Var("f"),
                Lambda(Var("z"), Apply(Var("f"), Apply(Var("n"), Apply(Var("f"), Var("z"))))
                )))

class PrettyPrinter {
  def apply(expr: Expr): String = {
    expr match {
      case Lambda(arg, body) => s"λ${apply(arg)}.${apply(body)}"
      case Apply(fun, arg) => s"(${apply(fun)} ${apply(arg)})"
      case Var(x) => s"$x"
    }
  }
}


import scala.util.parsing.combinator.syntactical.StdTokenParsers
import scala.util.parsing.combinator.lexical.StdLexical

class LambdaParser extends StdTokenParsers {
  type Tokens = StdLexical
  val lexical = new StdLexical
  lexical.delimiters ++= Seq("λ", ".", "(", ")")
}


val pr1 = new PrettyPrinter()
pr1(succ)
