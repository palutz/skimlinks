package io.palutz.domain

case class Developer(id: String, name: String, apiKey: String, expire: Long, enabled: Boolean)

case class Character(id: Int, name: String)